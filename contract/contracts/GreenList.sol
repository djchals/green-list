// SPDX-License-Identifier: MIT
pragma solidity = 0.8.13;

import "@openzeppelin/contracts/access/Ownable.sol";

contract GreenList is Ownable {
    mapping (address => bool) private greenList;
    uint unBlockDate;

    function setUnblockDate(uint _unBlockDate) public onlyOwner {
        unBlockDate = _unBlockDate;
    }
    function addToGreenList(address _wallet) public onlyOwner {
        greenList[_wallet] = true;
    }

    function removeFromGreenList(address _wallet) public onlyOwner {
        greenList[_wallet] = false;
    }
    function isListed(address _wallet) internal view onlyOwner returns (bool) {
        return greenList[_wallet];
    }

    function hasPermissionForDate(address _wallet) public view returns (bool) {
        if(isListed(_wallet)) {
            return true;
        //for the block.timestamp to be updated to now there must be tx in the blockchain and in ganache some will have to be done by hand
        }else if(block.timestamp >= unBlockDate){
            return true;
        }else{
            return false;
        }
    }
}