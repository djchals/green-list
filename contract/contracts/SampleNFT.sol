// SPDX-License-Identifier: MIT
pragma solidity = 0.8.13;

import "./ERC721A.sol";
import "./GreenList.sol";

contract SampleNFT is ERC721A, GreenList {

    uint256 public maxSupply;

    constructor(uint _unBlockDate, uint256 _maxSupply) ERC721A("Sample NFT", "Sample") GreenList() {
        maxSupply = _maxSupply;
        setUnblockDate(_unBlockDate);
    }

    function mint(address _buyer, uint256 _quantity) public {
        require(maxSupply - _totalMinted() >= _quantity, "Sample NFT: max supply reached");
        require(hasPermissionForDate(_buyer), "Sample NFT: You are not greenlisted");
        _safeMint(_buyer, _quantity);
    }
}