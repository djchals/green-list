const SampleNFT = artifacts.require('SampleNFT');
module.exports = function (deployer) {
  //When arrive to this date mint will be open for all addresses (whether are on the list or not)
  const weekInSeconds = 604800;
  const unBlockDate = Math.round(weekInSeconds + (new Date().getTime())/1000);
  const maxSupply = 1000;

  deployer.deploy(SampleNFT, unBlockDate, maxSupply);
};
